#python
import random
def sysCall_thread():
#e.g. non-synchronized loop:
sim.setThreadAutomaticSwitch(True)
objHandle = sim.getObject("/target")
while True:
    p=sim.getObjectPosition(objHandle,-1)
    if (-2.5<p[0]<2.5):
        p[0]=p[0]+random.choice([-0.01,0.01])
    if (-2.5<p[1]<2.5):
        p[1]=p[1]+random.choice([-0.01,0.01])
    sim.setObjectPosition(objHandle,-1,p)
pass


The algorithm mainly depends on
1- Specify the displaying area's boundaries to keep the quadcopter playing in
by moving on the x access and the Y access. I have choce the boundaries to be (-2.5, 2.5 ) in order to be in the range
of the specified (5,6)
2- Moving randomly in an unexpected way.
Both goals were achieved by making p [0] on X and p[1] on the way inside the area.The increment of movement specified to be
chosen randomly.
In this way any intaders cannot detect the quadcopter while moving.
